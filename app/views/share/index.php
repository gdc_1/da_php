<?php
include_once 'app/views/share/header.php';
?>

<div class="row">
    <form>
    <div style="display: flex; justify-content: space-between;">
    <a href="/chieu2/product/add" class="btn btn-primary btn-icon-split">
        <span class="icon text-white-50">
            <i class="fas fa-flag"></i>
        </span>
        <span class="text">Add Product</span>
    </a>

    <a href="/chieu2/cart/show" class="btn btn-primary btn-icon-split">
        <span class="icon text-white-50">
            <i class="fas fa-flag"></i>
        </span>
        <span class="text">Cart</span>
    </a>
    </div>
    </form>
    <div class="col-sm-12">
        <table class="table table-bordered dataTable" id="dataTable" width="100%" cellspacing="0" role="grid" aria-describedby="dataTable_info" style="width: 100%;">
            <thead>
                <tr>
                    <th>Id</th>
                    <th>Name</th>
                    <th>Description</th>
                    <th>Image</th>
                    <th>Price</th>
                    <th>Action (Edit/Delete)</th>
                </tr>
            </thead>
            <tbody>
                <?php while ($row = $products->fetch(PDO::FETCH_ASSOC)) : ?>
                    <tr>
                        <th>
                            <?= $row['id'] ?>
                            <a class="btn btn-danger" href="/chieu2/cart/add/<?= $row['id'] ?>">ADD TO CART</a>
                        </th>
                        <th><?= $row['name'] ?></th>
                        <th><?= $row['description'] ?></th>
                        <th>

                            <?php
                            if (empty($row['image']) || !file_exists($row['image'])) {
                                echo "No Image!";
                            } else {
                                echo "<img src='/chieu2/" . $row['image'] . "' alt='' width='500' height='375'/>";
                            }
                            ?>

                        </th>
                        <th><?= $row['price'] ?></th>
                        <th>
                            <a href="/chieu2/product/edit/<?=$row['id']?>">
                                Edit
                            </a>
                        | Delete</th>
                    </tr>
                <?php endwhile; ?>
            </tbody>
        </table>
    </div>
</div>



<?php

include_once 'app/views/share/footer.php';

?>